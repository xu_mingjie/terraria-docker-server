# terraria-docker-server

泰拉瑞亚服务器Docker镜像.

之前和朋友玩泰拉瑞亚时，Steam的联机一直连不上，所以在服务器上搭了一个泰拉的主机，但是官方给的包只能用7777端口，和朋友玩时只能用一张图，所以自己构建了一个docker的泰拉瑞亚服务器镜像，可以用虚拟端口的方式在一台服务器上开启多个地图。
![泰拉瑞亚服务器Docker镜像](https://images.gitee.com/uploads/images/2021/0417/221649_4bb00b5b_7731863.png)

--------------

## 安装步骤

1. 安装配置Docker环境

2. 下载该仓库中文件

```
git clone "https://gitee.com/xu_mingjie/terraria-docker-server.git"
```
> `terraria/serverconfig.txt`此文件内容 `maxplayers` 是 最大联机人数200 ，`password`是 泰拉瑞亚服务器密码
```
maxplayers=200
password=docker
```


3. 在目录terraria-docker-server下执行build命令

```
docker build -t terraria-docker-server:lastest .
```

等待构建完成.


使用`docker images`命令查看terraria-docker-server镜像是否生成.


4. 创建容器

> 7778是自己服务器的端口，可自定义，最后别忘了去安全组放行该端口

```
docker run -id --name "terraria-server" -p 7778:7777 terraria-docker-server:lastest
```

如果没有异常，就可以打开游戏连接了


5. 进入服务器

服务器密码默认为 'docker'


### 其他

这里默认生成了一张最大的地图，叫做AntzUhl-Docker-World的地图，最大联机人数为200，普通难度，如果有特殊需求，可以在build之前自己修改[配置文件](terraria/serverconfig.txt)中的信息。
